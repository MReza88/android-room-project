package com.example.listapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import com.example.listapp.models.Cocktail;

public class EditCocktail extends AppCompatActivity {
    // Variables to manage the activity
    private long cocktailIndex;
    private CocktailDao cocktailDao;
    private Cocktail cocktailUpdate;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cocktail);

        // Set ActionBar title and initialize views    
        getSupportActionBar().setTitle("Edit Cocktail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final EditText editTextName = findViewById(R.id.edit_text_name);
        final EditText editTextInstructions = findViewById(R.id.edit_text_instructions);
        final EditText editTextImageUrl = findViewById(R.id.edit_text_image_url);
        Button saveButton = findViewById(R.id.button_save);

        // Database initialization
        AppDataBase db = AppDataBase.getInstance(this);
        cocktailDao = db.cocktailDao();

        // Retrieve cocktail ID from intent
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        cocktailIndex = intent.getLongExtra("cocktailIndex", -1);

        if (cocktailIndex < 0) {
            finish();
            return;
        }

        // Button click listener to save changes
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Update the Cocktail object with new details
                cocktailUpdate.setName(editTextName.getText().toString());
                cocktailUpdate.setInstructions(editTextInstructions.getText().toString());
                cocktailUpdate.setImageUrl(editTextImageUrl.getText().toString());

                // Finish the activity and update the Cocktail in the database
                finish();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        cocktailDao.updateCocktail(cocktailUpdate);
                    }
                }).start();
            }
        });

        // Observing changes in Cocktail details
        cocktailDao.getCocktailById(cocktailIndex).observe(this, new Observer<Cocktail>() {
            @Override
            public void onChanged(Cocktail cocktail) {
                // Set EditText fields with existing Cocktail details for editing
                editTextName.setText(cocktail.getName());
                editTextInstructions.setText(cocktail.getInstructions());
                editTextImageUrl.setText(cocktail.getImageUrl());
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                cocktailUpdate = cocktail; // Store the current Cocktail object for updating 
            }
        });
    }
}
