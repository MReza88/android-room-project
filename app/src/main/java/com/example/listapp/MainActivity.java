package com.example.listapp;

import com.example.listapp.models.Cocktail;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.listapp.models.CocktailWithReview;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private ListView listView;
    private CocktailDao cocktailDao;
    private CocktailAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Database initialization
        AppDataBase db = AppDataBase.getInstance(this);
        cocktailDao = db.cocktailDao();

        // Link ListView from layout
        listView = findViewById(R.id.list_view);

        // Observe the LiveData only once
        LiveData<List<CocktailWithReview>> cocktailsLiveData = cocktailDao.getAllCocktailsWithReviewCount();
        cocktailsLiveData.observe(this, cocktailWithReviews -> {
            // Update the ListView adapter with the new data after any changes
            if (adapter == null) {
                adapter = new CocktailAdapter(this, cocktailWithReviews);
                listView.setAdapter(adapter);
            } else {
                adapter.setCocktailList(cocktailWithReviews);
                adapter.notifyDataSetChanged();
            }
        });

        // Set ActionBar title
        getSupportActionBar().setTitle("Cocktail");

        // Set item click listener for ListView items
        listView.setOnItemClickListener((parent, view, position, id) -> {
            // Fetch the clicked Cocktail data and start the CocktailViewActivity
            CocktailWithReview cocktail = (CocktailWithReview) listView.getAdapter().getItem(position);
            Intent editIntent = new Intent(MainActivity.this, CocktailViewActivity.class);
            editIntent.putExtra("cocktailIndex", id);
            startActivity(editIntent);
        });


        // Set click listener for adding a new cocktail
        FloatingActionButton fabAddCocktail = findViewById(R.id.fab_add_cocktail);
        fabAddCocktail.setOnClickListener(view -> {
            // Start the SearchActivity to add a new cocktail
            Intent intent = new Intent(MainActivity.this, SearchActivity.class);
            intent.putExtra("cocktailName", "new cocktail");
            startActivity(intent);
        });
    }
}
