package com.example.listapp;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Button;
import android.os.Bundle;
import android.widget.ListView;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.listapp.models.Cocktail;
import com.example.listapp.models.CocktailWithReview;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class SearchActivity extends AppCompatActivity {
    private EditText searchField;
    private Button searchButton;
    private ListView searchResultsList;
    private Button addCocktailButton;
    private List<Cocktail> cocktailList = new ArrayList<Cocktail>();
    private SearchAdapter adapter;
    private CocktailDao cocktailDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set content view and find views by ID
        setContentView(R.layout.activity_search);
        searchField = findViewById(R.id.search_field);
        searchButton = findViewById(R.id.search_button);
        searchResultsList = findViewById(R.id.search_results_list);
        addCocktailButton = findViewById(R.id.add_cocktail_button);

        // Initialize the database and adapter
        AppDataBase db = AppDataBase.getInstance(this);
        cocktailDao = db.cocktailDao();
        adapter = new SearchAdapter(this, cocktailList);
        searchResultsList.setAdapter(adapter);

        // Search button click listener
        searchButton.setOnClickListener(view -> {
            String searchQuery = searchField.getText().toString();
            performCocktailSearch(searchQuery);
        });

        // Item click listener for search results list
        searchResultsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent viewIntent = new Intent(getApplicationContext(), MainActivity.class);
            Cocktail selectedCocktail = (Cocktail) cocktailList.get(position);
            Cocktail new_cocktail = new Cocktail();
            new_cocktail.setName(selectedCocktail.getName());
            new_cocktail.setInstructions(selectedCocktail.getInstructions());
            new_cocktail.setImageUrl(selectedCocktail.getImageUrl());

            // Add the selected cocktail to your app's list and open the view activite
            new Thread(new Runnable() {
                @Override
                public void run() {
                    cocktailDao.addCocktail(new_cocktail);
                }
            }).start();

            // Open the Cocktail View Activity
            startActivity(viewIntent);
            }
        });

        // Add cocktail button click listener
        addCocktailButton.setOnClickListener(view -> {
            // Open the Cocktail Edit Activity
            Intent editIntent = new Intent(SearchActivity.this, AddCocktailActivity.class);
            startActivity(editIntent);
        });
    }

    // Method to perform a cocktail search using an API
    private void performCocktailSearch (String query) {
        cocktailList.clear();

        String url = "https://www.thecocktaildb.com/api/json/v1/1/search.php?s=" + query;
        RequestQueue queue = Volley.newRequestQueue(this);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                response -> {
                    try {
                        JSONArray drinks = response.getJSONArray("drinks");

                        for (int i = 0; i < drinks.length(); i++) {
                            JSONObject drink = drinks.getJSONObject(i);
                            String drinkName = drink.getString("strDrink");
                            String instructions = drink.getString("strInstructions");
                            String thumbnail = drink.getString("strDrinkThumb");

                            // Create a Cocktail object from the retrieved data
                            Cocktail cocktail = new Cocktail();
                            cocktail.setName(drinkName);
                            cocktail.setInstructions(instructions);
                            cocktail.setImageUrl(thumbnail);

                            // Add the parsed cocktail to the list
                            this.cocktailList.add(cocktail);
                        }
                        // After parsing, add the parsed cocktails to your adapter
                        this.adapter.setSearchList(this.cocktailList);

                        // Notify the adapter that the data has changed
                        this.adapter.notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            Log.e("CocktailSearch", "Error during search", error);
            runOnUiThread(() -> {
                Toast.makeText(SearchActivity.this, "Error during search", Toast.LENGTH_SHORT).show();
            });
        });
        queue.add(jsonObjectRequest);
    }
}
