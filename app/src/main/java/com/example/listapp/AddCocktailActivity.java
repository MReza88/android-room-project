package com.example.listapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import com.example.listapp.models.Cocktail;
import com.squareup.picasso.Picasso;


public class AddCocktailActivity extends AppCompatActivity {
    private CocktailDao cocktailDao;

    @Override
    protected void onCreate( Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cocktail);

        // Get an instance of the database and initialize the DAO
        AppDataBase db = AppDataBase.getInstance(this);
        cocktailDao = db.cocktailDao();

        // Set the title and enable the back button on the ActionBar
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Add New Cocktail");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // Initialize EditText fields for name, instructions, and image URL
        final EditText editTextName = findViewById(R.id.edit_text_name);
        final EditText editTextInstructions = findViewById(R.id.edit_text_instructions);
        final EditText editTextImageUrl = findViewById(R.id.edit_text_image_url);

        // Button to save the new cocktail details
        Button saveButton = findViewById(R.id.button_save);
        saveButton.setOnClickListener(view -> {
            // Get the values from EditText fields
            String newCocktailName = editTextName.getText().toString();
            String newCocktailInstruction = editTextInstructions.getText().toString();
            String imageUrl = editTextImageUrl.getText().toString();

            // Create a new Cocktail object with the entered details
            cocktailDao = db.cocktailDao();
            Cocktail cocktail = new Cocktail();
            cocktail.setName(newCocktailName);
            cocktail.setInstructions(newCocktailInstruction);
            cocktail.setImageUrl(imageUrl);

            // Insert the new cocktail into the database using a new thread
            new Thread(new Runnable() {
                @Override
                public void run() {
                    cocktailDao.addCocktail(cocktail);
                }
            }).start();

            // Navigate back to the MainActivity after adding the cocktail
            Intent content = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(content);
        });
    }
}





