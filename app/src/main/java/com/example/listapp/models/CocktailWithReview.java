package com.example.listapp.models;

import androidx.room.ColumnInfo;
import androidx.room.Embedded;

// This class represents a Cocktail with its associated review count
public class CocktailWithReview {

    @Embedded
    public Cocktail cocktail; // Embedded Cocktail object

    @ColumnInfo (name = "reviewCount")
    public int reviewCount; // Number of reviews associated with the cocktail

    // Constructor to initialize CocktailWithReview object with a Cocktail and its review count
    public CocktailWithReview(Cocktail cocktail, int reviewCount) {
        this.cocktail = cocktail;
        this.reviewCount = reviewCount;
    }

    // Getter and Setter for the embedded Cocktail object
    public Cocktail getCocktail() {
        return cocktail;
    }

    public void setCocktail(Cocktail cocktail) {
        this.cocktail = cocktail;
    }

    // Getter for the review count associated with the Cocktail
    public int getReviews() {
        return reviewCount;
    }
}
