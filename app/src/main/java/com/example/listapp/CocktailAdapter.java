package com.example.listapp;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.listapp.models.Cocktail;
import com.example.listapp.models.CocktailWithReview;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CocktailAdapter extends BaseAdapter {
    private List<CocktailWithReview> cocktailList;
    private LayoutInflater inflater;
    private Context context;

    // Constructor to initialize the adapter with data and context
    public CocktailAdapter(Context context, List<CocktailWithReview> cocktailList) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.cocktailList = cocktailList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return cocktailList.size();
    }

    @Override
    public Object getItem(int position) {
        return cocktailList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return cocktailList.get(position).cocktail.getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.cocktail_adapter, null);
            holder = new ViewHolder();
            holder.cocktailName = convertView.findViewById(R.id.text_cocktail_name);
            holder.cocktailImage = convertView.findViewById(R.id.image_cocktail);
            holder.cocktailReviewText = convertView.findViewById(R.id.cocktail_review_text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // Retrieve the CocktailWithReview object at the given position
        CocktailWithReview cocktail = cocktailList.get(position);

        // Set the cocktail name in the TextView
        holder.cocktailName.setText(cocktail.cocktail.getName());
        
        // Load the cocktail image using Picasso or set a default image resource        
        if (!TextUtils.isEmpty(cocktail.cocktail.getImageUrl())) {
            Picasso.with(this.context).load(cocktail.cocktail.getImageUrl()).into(holder.cocktailImage);
        } else {
            holder.cocktailImage.setImageResource(cocktail.cocktail.getCocktailImage());
        }

        // Display the number of reviews for the cocktail in the TextView
        int reviewCount = cocktail.getReviews();
        String reviewText;
        if (reviewCount == 1) {
            reviewText = "review";
        } else {
            reviewText = "reviews";
        }
        holder.cocktailReviewText.setText(String.valueOf(reviewCount) + " " + reviewText);
        return convertView;
    }

    // Method to update the cocktail list and notify data changes
    public void setCocktailList(List<CocktailWithReview> cocktailList) {
    this.cocktailList = cocktailList;
    notifyDataSetChanged();
    }

    // ViewHolder pattern to improve ListView performance
    static class ViewHolder {
        TextView cocktailName;
        ImageView cocktailImage;
        TextView cocktailReviewText;
    }
}
