package com.example.listapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Insert;
import androidx.room.Update;
import androidx.room.Delete;

import com.example.listapp.models.Cocktail;
import com.example.listapp.models.CocktailWithReview;
import com.example.listapp.models.Review;

import java.util.List;

@Dao
public interface CocktailDao {

    // Retrieve all cocktails with review count using LiveData
    @Query("select cocktail.*, count(review.cocktailId) as reviewCount, max(review.reviewText) reviewText from cocktail left join review on cocktail.id=review.cocktailId group by cocktail.id")
    LiveData<List<CocktailWithReview>> getAllCocktailsWithReviewCount();

    // Retrieve a cocktail by its ID using LiveData
    @Query("select * from cocktail where  id = :id")
    LiveData<Cocktail> getCocktailById(long id);

    // Retrieve reviews for a specific cocktail using its ID with LiveData
    @Query("select * from review where cocktailId = :id")
    LiveData<List<Review>> getReviews(long id);

    // Delete reviews associated with a specific cocktail using its ID
    @Query("delete from review where cocktailId = :cocktailId")
    void deleteReviewForCocktail(long cocktailId);

    // Insert a new cocktail into the database
    @Insert
    void addCocktail(Cocktail cocktail);

    // Insert a new review into the database
    @Insert
    void addReview(Review review);

    // Update an existing cocktail in the database
    @Update
    void updateCocktail(Cocktail cocktail);

    // Delete a specific cocktail from the database
    @Delete
    void deleteCocktail(Cocktail cocktail);
}
