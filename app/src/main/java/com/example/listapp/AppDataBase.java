package com.example.listapp;

import android.content.Context;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.Room;

import com.example.listapp.models.Cocktail;
import com.example.listapp.models.Review;

// Database annotation to declare entities and database version
@Database(entities = {Cocktail.class, Review.class}, version = 15)
public abstract class AppDataBase extends RoomDatabase {

    // Abstract method to retrieve the DAO (Data Access Object)
    public abstract CocktailDao cocktailDao();

    // Singleton instance of the database
    static AppDataBase instance;
    
    // Singleton pattern to get an instance of the database
    static synchronized public AppDataBase getInstance(Context context) {
        if (instance==null) {
            // Create a new instance of the database using Room's databaseBuilder
            instance = Room.databaseBuilder(context.getApplicationContext(),
                            AppDataBase.class, "cocktail-database")
                    .fallbackToDestructiveMigration() // Handles database migration by dropping and recreating the database if necessary
                    .build();
        } return instance;
    }
}
