package com.example.listapp;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.listapp.models.Cocktail;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SearchAdapter extends BaseAdapter {
    private List<Cocktail> cocktailList;
    private LayoutInflater inflater;
    private Context context;

    public SearchAdapter(Context context, List<Cocktail> cocktailList) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.cocktailList = cocktailList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return cocktailList.size();
    }

    @Override
    public Object getItem(int position) {
        return cocktailList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            // Inflate the layout if convertView is null
            convertView = inflater.inflate(R.layout.cocktail_adapter, null);
            holder = new ViewHolder();
            holder.cocktailName = convertView.findViewById(R.id.text_cocktail_name);
            holder.cocktailImage = convertView.findViewById(R.id.image_cocktail);
            holder.cocktailReviewText = convertView.findViewById(R.id.cocktail_review_text);
            convertView.setTag(holder);
        } else {
            // Reuse the ViewHolder if convertView exists
            holder = (ViewHolder) convertView.getTag();
        }

        // Get the Cocktail object for the current position
        Cocktail cocktail = cocktailList.get(position);

        // Set the cocktail name to the corresponding TextView
        holder.cocktailName.setText(cocktail.getName());

        // Load the cocktail image using Picasso library into the ImageView    
        if (!TextUtils.isEmpty(cocktail.getImageUrl())) {
            Picasso.with(this.context).load(cocktail.getImageUrl()).into(holder.cocktailImage);
        } else {
            // If no image URL is available, set a default image
            holder.cocktailImage.setImageResource(cocktail.getCocktailImage());
        }

        return convertView;// Return the populated view for the ListView
    }

    // Method to set the updated list of cocktails
    public void setSearchList(List<Cocktail> cocktailList) {
        this.cocktailList = cocktailList;
    }

    // ViewHolder pattern to improve ListView performance by reusing views
    static class ViewHolder {
        TextView cocktailName;
        ImageView cocktailImage;
        TextView cocktailReviewText;
    }
}
