package com.example.listapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.listapp.models.Review;

import java.util.List;

public class ReviewAdapter extends BaseAdapter {
    private List<Review> reviewList;
    private LayoutInflater inflater;

    // Constructor to initialize the ReviewAdapter
    public ReviewAdapter(Context context, List<Review> reviews) {
        inflater = LayoutInflater.from(context);
        reviewList = reviews;
    }

    // Get the total number of reviews
    @Override
    public int getCount() {
        return reviewList.size();
    }

    // Get a specific review at a given position
    @Override
    public Object getItem(int position) {
        return reviewList.get(position);
    }

    // Get the item ID for a given position
    @Override
    public long getItemId(int position) {
        return position;
    }

    // Create or reuse views to display the reviews
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            // Inflate layout for a review item if it's null
            convertView = inflater.inflate(R.layout.review_item, parent, false);
            holder = new ViewHolder();
            holder.reviewText = convertView.findViewById(R.id.reviewText);
            holder.reviewName = convertView.findViewById(R.id.reviewName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // Set review data to the views within the review item
        Review review = reviewList.get(position);
        holder.reviewText.setText(review.getReviewText());
        holder.reviewName.setText(review.getReviewName());
        return convertView;
    }

    // ViewHolder pattern to hold the review item views
    static class ViewHolder {
        TextView reviewText;
        TextView reviewName;
    }
}

