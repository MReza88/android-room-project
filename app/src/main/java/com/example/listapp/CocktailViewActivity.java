package com.example.listapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.listapp.models.Cocktail;
import com.example.listapp.models.CocktailWithReview;
import com.example.listapp.models.Review;
import com.squareup.picasso.Picasso;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;

import java.util.ArrayList;
import java.util.List;


public class CocktailViewActivity extends AppCompatActivity {
    private long cocktailIndex;
    private ListView reviewsList;
    private CocktailDao cocktailDao;
    private Cocktail selected;
    private List<Review> reviewList = new ArrayList<Review>();
    private ReviewAdapter adapter;

    private EditText reviewNameEditText;
    private EditText reviewDescriptionEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cocktail_view);

        // Views initialization
        TextView instructionsTextView = findViewById(R.id.text_view_cocktail_instructions);
        ImageView cocktailImageView = findViewById(R.id.image_view_cocktail);

        // EditTexts for review details
        reviewNameEditText = findViewById(R.id.reviewName);
        reviewDescriptionEditText = findViewById(R.id.reviewDescription);

        // Database initialization
        AppDataBase db = AppDataBase.getInstance(this);
        cocktailDao = db.cocktailDao();


        // Review list adapter initialization
        adapter = new ReviewAdapter(this, reviewList);
        reviewsList = findViewById(R.id.reviewsLayout);
        reviewsList.setAdapter(adapter);

        // Retrieve cocktail ID from intent
        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        cocktailIndex = intent.getLongExtra("cocktailIndex", -1);

        if (cocktailIndex < 0){
            finish();
            return;
        }

        // Observers for cocktail and review data
        cocktailDao.getCocktailById(cocktailIndex).observe(this, new Observer<Cocktail>() {
            @Override
            public void onChanged(Cocktail cocktail) {
                // Update UI with cocktail details
                if (cocktail != null) {
                    selected = cocktail;
                    getSupportActionBar().setTitle(cocktail.getName());
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

                    instructionsTextView.setText(cocktail.getInstructions());
                    // If you are using an image URL:
                    String imageUrl = cocktail.getImageUrl();
                    if (imageUrl != null && !imageUrl.isEmpty()) {
                        Picasso.with(getApplicationContext()).load(imageUrl).into(cocktailImageView);
                    } else {
                        // If you're using local drawable resources
                        cocktailImageView.setImageResource(cocktail.getCocktailImage());
                    }
                }
                else {
                    finish();
                }
            }
        });

        cocktailDao.getReviews(cocktailIndex).observe(this, new Observer<List<Review>>() {
            @Override
            public void onChanged(List<Review> reviews) {
                // Update review list
                    reviewList.clear();
                    reviewList.addAll(reviews);
                    adapter.notifyDataSetChanged();
            }
        });


        // Button click listener to add reviews
        Button addReviewButton = findViewById(R.id.reviewButton);
        addReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Retrieve review details from EditText fields
                String reviewName = reviewNameEditText.getText().toString();
                String reviewDescription = reviewDescriptionEditText.getText().toString();

                // Check if both reviewName and reviewDescription are not empty before adding the review
                if (!reviewName.isEmpty() && !reviewDescription.isEmpty()) {
                    // Add the review to the database using cocktailDao
                    Review review = new Review(reviewName, reviewDescription, selected.getId()); // 'selected' is your selected Cocktail object

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            cocktailDao.addReview(review);
                        }
                    }).start();
                    // Optionally, display a message or perform additional actions after adding the review
                    // show a toast indicating that the review has been added
                    Toast.makeText(CocktailViewActivity.this, "Review added!", Toast.LENGTH_SHORT).show();

                    // Clear the EditText fields after adding the review
                    reviewNameEditText.setText("");
                    reviewDescriptionEditText.setText("");
                } else {
                    // If reviewName or reviewDescription is empty, prompt the user to fill in both fields
                    Toast.makeText(CocktailViewActivity.this, "Please enter both review name and description", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    // Inflate the menu layout to the ActionBar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    // Handle the ActionBar menu item click
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_edit) {
            Intent editIntent = new Intent(CocktailViewActivity.this, EditCocktail.class);
            editIntent.putExtra("cocktailIndex", cocktailIndex);
            startActivity(editIntent);
            return true;
        } else if (item.getItemId() == R.id.action_delete) {
            deleteCocktailAndReviews();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteCocktailAndReviews() {
        if (selected != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    // Delete reviews associated with the cocktail using DAO method
                    cocktailDao.deleteReviewForCocktail(selected.getId());
                    // Delete the cocktail itself using DAO method
                    cocktailDao.deleteCocktail(selected);
                }
            }).start();
        }finish();// Finish the activity after deletion
    }
}