package com.example.listapp.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
// this class represents a Review entity stored in the database
public class Review {
    @PrimaryKey(autoGenerate = true)
    public long id; // unique identifier for the review
    private String reviewName; // name of the review
    private String reviewText; // text of the review
    private long cocktailId; // ID of the associated cocktail


    // constructor to initialize a Review object with reviewName, reviewText, and cocktailId
    public Review(String reviewName, String reviewText, long cocktailId) {
        this.reviewName = reviewName;
        this.reviewText = reviewText;
        this.cocktailId = cocktailId;
    }

    // getter for the review text
    public String getReviewText() {
        return reviewText;
    }
 
    // getter for the review name
    public String getReviewName() {
        return reviewName;
    }

    // getter for the review name
    public long getCocktailId() {
        return  cocktailId;
    }
}
