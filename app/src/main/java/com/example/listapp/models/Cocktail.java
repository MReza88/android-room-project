package com.example.listapp.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.listapp.R;
import androidx.room.TypeConverter;

@Entity
public class Cocktail {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String name;
    private String instructions;
    private String imageUrl;

    // Getter and Setter for 'id'
    public long getId() {
        return id;
    }
    public void setId(long id) { 
        this.id = id; 
    }

    // Getter and Setter for 'name'
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    // Getter and Setter for 'instructions'
    public String getInstructions(){
        return instructions;
    }
    public void setInstructions(String instructions){
        this.instructions = instructions;
    }


    // Getter and Setter for 'imageUrl'
    public void setImageUrl(String imageUrl){
        this.imageUrl = imageUrl;
    }
    public String getImageUrl() {
        return imageUrl;
    }

    // Method to assign image based on the name of the cocktail
    public int getCocktailImage() {

        String name = this.name.toLowerCase();
        if (name.contains("red")) {
            return R.drawable.red;
        } else if (name.contains("orange")) {
            return R.drawable.orange;
        } else if (name.contains("yellow")) {
            return R.drawable.yellow;
        } else if (name.contains("green")) {
            return R.drawable.green;
        } else if (name.contains("blue")) {
            return R.drawable.blue;
        } else if (name.contains("purple")) {
            return R.drawable.purple;
        }
        return R.drawable.white;
    };

    // Override toString() method to represent object details as a string
    @Override
    public String toString() {
        return "Cocktail{" +
                "name='" + name + '\'' +
                ", instructions='" + instructions + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
